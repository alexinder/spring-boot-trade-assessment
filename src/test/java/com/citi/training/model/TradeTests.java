package com.citi.training.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradeTests {
	
	private long testId = 1;
	private String testStock = "AAPL";
	private long testBuy = 0;
	private double testPrice = 1.16;
	private long testVolume = 235;
	
	@Test
    public void test_Trade_defaultConstructorAndSetters() {
        
		Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
        
        assertEquals("Trade id should be equal to testId", testId, testTrade.getId());
        assertEquals("Trade stock should be equal to testStock", testStock, testTrade.getStock());
        assertEquals("Trade buy value should be equal to testBuy", testBuy, testTrade.getBuy());        
        assertEquals("Trade price value should be equal to testPrice", testPrice, testTrade.getPrice(), 0.00001);
        assertEquals("Trade volume value should be equal to testVolume", testVolume, testTrade.getVolume());
	}
	
	@Test
    public void test_Trade_valueConstructorAndSetters() {
        
		Trade testTrade = new Trade();
        
		testTrade.setId(testId);
        testTrade.setStock(testStock);
        testTrade.setBuy(testBuy);
        testTrade.setPrice(testPrice);
        testTrade.setVolume(testVolume);
        
        assertEquals("Trade id should be equal to testId", testId, testTrade.getId());
        assertEquals("Trade stock should be equal to testStock", testStock, testTrade.getStock());
        assertEquals("Trade buy value should be equal to testBuy", testBuy, testTrade.getBuy());        
        assertEquals("Trade price value should be equal to testPrice", testPrice, testTrade.getPrice(), 0.00001);
        assertEquals("Trade volume value should be equal to testVolume", testVolume, testTrade.getVolume());
	}
	
	@Test
	public void test_Product_toString() {
		
		Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
		
		assertTrue("toString should contain testId", testTrade.toString().contains(Long.toString(testId)));
		assertTrue("toString should contain testStock", testTrade.toString().contains(testStock));
		assertTrue("toString should contain testBuy", testTrade.toString().contains(Long.toString(testBuy)));
		assertTrue("toString should contain testPrice", testTrade.toString().contains(Double.toString(testPrice)));
		assertTrue("toString should contain testVolume", testTrade.toString().contains(Long.toString(testVolume)));
	}
	
}
