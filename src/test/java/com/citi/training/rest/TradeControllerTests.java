package com.citi.training.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.dao.TradeDao;
import com.citi.training.model.Trade;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
public class TradeControllerTests {

    private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeDao tradeDaoObject;

    @Test
    public void findAllTrades_returnsList() throws Exception {
        when(tradeDaoObject.findAll()).thenReturn(new ArrayList<Trade>());

        MvcResult result = this.mockMvc.perform(get("/trade")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from TradeDaoObject.findAll: " +
                    result.getResponse().getContentAsString());
    }

    @Test
    public void getTradeById_returnsOK() throws Exception {
        Trade testTrade = new Trade(5, "AAPL", 0, 5.43, 200);

        when(tradeDaoObject.findById(testTrade.getId())).thenReturn(
                                                    Optional.of(testTrade));

        MvcResult result = this.mockMvc.perform(get("/trade/" + testTrade.getId()))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from TradeDaoObject.getTrade: " +
                    result.getResponse().getContentAsString());
    }
    
    @Test
    public void deleteById_returnsVoid() throws Exception {

        this.mockMvc.perform(delete("/trade/1")).andExpect(status().isNoContent());

        logger.info("Delete was successful");
    }

    
}

