package com.citi.training.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.model.Trade;

public interface TradeDao extends CrudRepository<Trade, Long> {

}
