package com.citi.training.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.citi.training.dao.TradeDao;
import com.citi.training.model.Trade;

@RestController
@RequestMapping("/trade")

public class TradeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
	
	@Autowired
    private TradeDao tradeDaoObject;
	
	@RequestMapping(method=RequestMethod.GET)
    public Iterable<Trade> findAll() {
        LOG.info("HTTP GET findAll()");
        return tradeDaoObject.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Trade findById(@PathVariable long id) {
        LOG.info("HTTP GET findById() id=[" + id + "]");
        return tradeDaoObject.findById(id).get();
    }

    @RequestMapping(method=RequestMethod.POST)
    public HttpEntity<Trade> save(@RequestBody Trade trade) {
        LOG.info("HTTP POST save() trade=[" + trade + "]");
        return new ResponseEntity<Trade>(tradeDaoObject.save(trade), HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value=HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id) {
        LOG.info("HTTP DELETE delete() id=[" + id + "]");
        tradeDaoObject.deleteById(id);
    }
	
}
