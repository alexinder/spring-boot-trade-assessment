package com.citi.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTradeAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTradeAssessmentApplication.class, args);
	}

}
