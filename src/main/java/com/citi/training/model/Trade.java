package com.citi.training.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * This Trade Class creates the trade object and associated constructor. Further, it contains the getter and setter methods.
 * @author Alexinder Riyat
 * @see TradeController
 * @see TradeTests
 * 
 *
 */


@Entity
public class Trade {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String stock;
	private long buy;
	private double price;
	private long volume;
	
	public Trade() {
		
	}
	
	public Trade(long id, String stock, long buy, double price, long volume) {
		this.id = id;
		this.stock = stock;
		this.buy = buy;
		this.price = price;
		this.volume = volume;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public long getBuy() {
		return buy;
	}
	public void setBuy(long buy) {
		this.buy = buy;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public long getVolume() {
		return volume;
	}
	public void setVolume(long volume) {
		this.volume = volume;
	}
	
	/**
	 * This is the toString() method to print the info for a Trade.
	 */
	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", buy=" + buy + ", price=" + price + ", volume=" + volume
				+ "]";
	}

}
